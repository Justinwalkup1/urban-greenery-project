# Urban Geenery Project Simulations
---
### Table of Contents
1. [Overview](#overview)
2. [Requirements](#requirements)
3. [Installing](#installing)
4. [Workflow](#workflow)
5. [Geometry](#geometry)
6. [Mesh](#mesh)
7. [Initial Conditions](#initial-conditions)
1. [Results](#results)
---
## Overview
In this file, we describe the workflow of how the Urban Greenery Project simulations were initialized, the results of the simulations, and how these simulations may be modified for different sizes of forests. 

The forest for the Urban Greenery Projects will be modeled as a radially symmetric geometry with the forest at the center and evenly spaced buildings on the exterior. Wind with a uniform velocity profile will blow from one direction. A render of the goemetry is shown below. 
**Note**: Render is for understanding purposes only. It is not to scale and not all buildings are shown. 
![Render of Urban Greenery Project Simulation](./images/urban%20greenery%20render.png)*Fig.1 Render of the domain in Fluent*

The radially symmetric nature of the geometry allows us to define a characteristic plane. In other words, we can simulate a 2D geometry that accurately represents the 3D geometry. A diagram of the 2D geometry is shown below. 
**Note**: Diagram is not to scale.
![Diagram of Urban Greenery Project Simulation](./images/urban%20greenery%20diagram.png)*Fig.2 Diagram of the domain in Fluent*

The buildings will be modeled as squares and the forest will have a corresponding drag force due to the leaves. The equation for drag is represented by the $f_D$ in the equation shown below. 
$$\frac{\partial u_i}{\partial t}+u_j\frac{\partial u_i}{\partial x_j}=f_D-\frac{1}{\rho}\frac{\partial p}{\partial x_i}+\nu\frac{\partial^2u_i}{\partial x_j\partial x_j}$$
The drag force of the forest can be represented by the equation below. 
$$f_D=C_D\hat{\lambda}_F||\hat{u}||\hat{u}_i$$

## Requirements 
The simulation for the Urban Greenery Project was conducted with `Ansys Fluent 2021 R1`

## Installing 
For information on installing Ansys Fluent, please refer to this [link](https://www.ansys.com/products/fluids/ansys-fluent). 

Simulationn files for this project are provided in the folder `./models`. In this folder, the simulations have been separated by the size of the forest. 

## Workflow
1. Geometry is created in `DesignModeler`.
2. Mesh is created in `Meshing`.
3. Initial conditions are setup in `Fluent`.
4. Results are analyzed in `CFD-Post`

This workflow can be viewed by openeing each simulation in `Workbench`. The Workbench interface allows users to open each step of the simulation. 

## Geometry
For the purpose of Urban Forest Project, parameters have been initialized so that the geometry can be modified without having to open the model in the `DesignModeler`. The parameters have been set so that the only parameter that is to be modified is the width of the forest. The parameters may be modified by opening the tab labeled `Parameter Set` in the workbench. The width of the forest is changed by modifying the parameter `Forest_width`. The value is a dimensionless integer that corresponds to the number of (building widths + street widths) that the forest occupies. Since each building is 10m and each street is 10m, the length of the forest can be determined with the following equation. 
$$\text{Forest\char`_width}*(10\text{m} + 10\text{m})=100\text{m} $$
For example, a forest with a width of `5` uses 5 building and street spaces and is 100m long.

**Note:** If for any instance, the dimensions must be modified further than parameterized, the geometry may be opened through the `Geometry` tab in the workbench. 

## Mesh
The mesh for the simulation has been preset so that once the parameters have been set, a mesh can be created using the `Update All Design Parts` button. A quadrilateral dominant mesh with an element size of 1m will be created. 

**Note:** If for any instance, the mesh must be modified further than preset, the mesh may be opened through the `Mesh` tab in the workbench. 

Named 

## Initial Conditions
#### Fluent Launcher
To view and modify the initial conditions for the simulation, open the tab labeled `Setup` in the workbench. This will open `Fluent Launcher`. The button for `2D` will automatically be selected since our geometry is two dimensional. Under the options section, select `Double Precision` and `Display Mesh After Reading`. 
**Optional**: If details of the machine are known, the number of `Solver Processes` may be set. Additionally, in the `Parallel Settings` tab, the `MPI Types` may be set. 

Click `Start With Selected Options` to start Fluent. 

#### Fluent

###### General: 
`Mesh -> Check` 
`Y Gravity Acceleration ->` $-9.81 \text{m/s}^2$
###### Models:
`Energy -> on`
`Viscous -> Standard k-omega`
###### Materials
    Fluid 
        air -> ideal gas
######
    Solid 
        concrete
###### Cell Zone Conditions
    Forest 
        • Source term: X-momentum: drag
        • Fixed Values: Temperature: 25[C]
###### Boundary Conditions
    Inlet 
        • Magnitude -> Velocity Magnitude: 1m/s
        • Thermal -> Temperature: 30[C]
#####
    Outlet
        • Backflow Total Temperature: 30[C]
#####
    Wall
        Buildings
            Thermal -> Temperature: 35[C]
                    -> Material Name: concrete
#####
    Mesh Interfaces
        int:01:interface4::interface1
        int02:interface5::interface2
        int03:interface6::interface3
#####
`Named Expressions:` $\text{drag}=-0.2*\text{Velocity.mag}*\text{Velocity.x}*0.1[\text{kg m}^{-4}]$

---
## Results
...

## References
